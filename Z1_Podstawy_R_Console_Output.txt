> # a
+ a <- 20 / log(2.78)
+ b <- a * 3
+ min(a, b)
[1] 19.56084

#c
> a <- 8:75
+ mean(a ^ 2)
[1] 2107.5

#d
> apropos("plot", mode = "function")
 [1] "assocplot"                    "barplot"                      "barplot.default"              "biplot"                       "boxplot"                      "boxplot.default"              "boxplot.matrix"               "boxplot.stats"               
 [9] "bwplot"                       "cdplot"                       "contourplot"                  "coplot"                       "densityplot"                  "dotplot"                      "fourfoldplot"                 "interaction.plot"            
[17] "lag.plot"                     "levelplot"                    "lplot.xy"                     "matplot"                      "monthplot"                    "mosaicplot"                   "panel.bwplot"                 "panel.contourplot"           
[25] "panel.densityplot"            "panel.dotplot"                "panel.levelplot"              "panel.levelplot.raster"       "panel.stripplot"              "panel.xyplot"                 "parallelplot"                 "plot"                        
[33] "plot.default"                 "plot.design"                  "plot.ecdf"                    "plot.function"                "plot.new"                     "plot.spec.coherency"          "plot.spec.phase"              "plot.stepfun"                
[41] "plot.ts"                      "plot.window"                  "plot.xy"                      "plotcp"                       "prepanel.default.bwplot"      "prepanel.default.densityplot" "prepanel.default.levelplot"   "prepanel.default.xyplot"     
[49] "preplot"                      "qqplot"                       "recordPlot"                   "replayPlot"                   "revoPlot"                     "rxLinePlot"                   "rxStepPlot"                   "rxVarImpPlot"                
[57] "savePlot"                     "screeplot"                    "spineplot"                    "stripplot"                    "sunflowerplot"                "termplot"                     "ts.plot"                      "xyplot"                      
[65] "xyplot.ts"

#e
> setwd("C:/Users/Piotr/Documents/Studia/Uczenie maszynowe/Z1_Podstawy_R")
+ a <- "tablet"
+ save(a, file = "tablet.RData")
+ remove(a)
+ load("tablet.RData")
+ a
[1] "tablet"

#f
> install.packages("gridExtra")
+ library(gridExtra)
+ help(package = "gridExtra")
+ grid.table(volcano[1:10,])
trying URL 'https://mran.revolutionanalytics.com/snapshot/2016-11-01/bin/windows/contrib/3.3/gridExtra_2.2.1.zip'
Content type 'application/zip' length 483331 bytes (472 KB)
downloaded 472 KB

package �gridExtra� successfully unpacked and MD5 sums checked

The downloaded binary packages are in
	C:\Windows\Temp\Rtmpk5K5il\downloaded_packages
starting httpd help server ... wykonano

#g
> seq(1000, 200, -8)
  [1] 1000  992  984  976  968  960  952  944  936  928  920  912  904  896  888  880  872  864  856  848  840  832  824  816  808  800  792  784  776  768  760  752  744  736  728  720  712  704  696  688  680  672  664  656  648  640  632  624  616  608  600
 [52]  592  584  576  568  560  552  544  536  528  520  512  504  496  488  480  472  464  456  448  440  432  424  416  408  400  392  384  376  368  360  352  344  336  328  320  312  304  296  288  280  272  264  256  248  240  232  224  216  208  200

#h

> a <- 50:30
+ b <- 4:50
+ d <- c(b, a)
+ d
 [1]  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 50 49 48 47 46 45 44 43 42 41 40 39 38 37 36 35 34 33 32 31 30


#i
> nazwa <- c("iPad Pro??", "iPad Air 2", "iPad Air", "4th generation iPad", "3rd generation iPad", "iPad 2", "iPad mini 4", "iPad mini 3", "iPad mini 2", "iPad mini")
+ modem <- c("GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/HSPA", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/LTE")
+ wy�wietlacz <- c("IPS TFT 12.9", "IPS LCD 9.7", "IPS LCD 9.7", "IPS LCD 9.7", "IPS LCD 9.7", "IPS LCD 9.7", "IPS LCD 7.9", "IPS LCD 7.9", "IPS LCD 7.9", "IPS LCD 7.9")
+ pami��_RAM <- c("4 GB", "2 GB", "1 GB", "1 GB", "1 GB", "512 MB", "2 GB", "1 GB", "1 GB", "512 MB")
+ pami��_wbudowana <- c("128 GB", "128 GB", "64 GB", "64 GB", "32 GB", "32 GB", "64 GB", "64 GB", "64 GB", "32 GB")
+ cena <- c(900, 440, 350, 500, 400, 370, 360, 400, 270, 300)
+ liczba_opinii <- c(51, 25, 265, 87, 427, 34, 98, 210, 12, 45)
+ tablety <- data.frame(nazwa, modem, wy�wietlacz, pami��_RAM, pami��_wbudowana, cena, liczba_opinii, stringsAsFactors = FALSE)
+ mean(tablety$cena)
[1] 429

#j
> dane_tabletu <- data.frame(nazwa = "iPad Pro 32", modem = "GSM", wy�wietlacz = "IPS LCD 9", pami��_RAM = "1 GB", pami��_wbudowana = "64 GB", cena = 450, liczba_opinii = 350)
+ tablety <- rbind(tablety, dane_tabletu)
+ mean(tablety$cena)
[1] 430.9091

#k
> oceny <- c(5, 4, 5, 5, 3.5, 5, 4, 5, 3.5, 4.5, 5)
+ tablety <- cbind(tablety, oceny)
+ tapply(tablety$cena, tablety$oceny, mean)
3.5   4 4.5   5 

335 400 300 495

#l
> nazwa <- c("iPad Pro?? 64", "iPad Air 128??", "iPad Air 2 128??", "iPad 2 128??")
+ modem <- c("GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/CDMA/HSPA/EVDO/LTE", "GSM/HSPA")
+ wy�wietlacz <- c("IPS TFT 12.9", "IPS LCD 9.7", "IPS LCD 9.7", "IPS LCD 9.7")
+ pami��_RAM <- c("4 GB", "2 GB", "1 GB", "1 GB")
+ pami��_wbudowana <- c("64 GB", "128 GB", "128 GB", "128 GB")
+ cena <- c(850, 480, 400, 500)
+ liczba_opinii <- c(87, 725, 660, 520)
+ oceny <- c(4, 4, 2, 5)
+ nowe_tablety <- data.frame(nazwa, modem, wy�wietlacz, pami��_RAM, pami��_wbudowana, cena, liczba_opinii, oceny)
+ tablety <- rbind(tablety, nowe_tablety)
+ liczebnosc <- table(tablety$oceny)
+ liczebnosc
+ barplot(liczebnosc)

2 3.5   4 4.5   5 
  
1   2   4   1   7

#m
> liczebnosc <- table(tablety$oceny)
+ procenty <- liczebnosc / sum(liczebnosc)
+ pie(procenty)
+ install.packages("plotrix")
+ library(plotrix)
+ fan.plot(liczebnosc, labels = names(liczebnosc))
trying URL 'https://mran.revolutionanalytics.com/snapshot/2016-11-01/bin/windows/contrib/3.3/plotrix_3.6-3.zip'
Content type 'application/zip' length 651737 bytes (636 KB)
downloaded 636 KB

package �plotrix� successfully unpacked and MD5 sums checked

The downloaded binary packages are in
	C:\Windows\Temp\Rtmpk5K5il\downloaded_packages

#n
> tablety[, "status_opinii"] <- ifelse(tablety$liczba_opinii == 0, "nie ma",
+                                            ifelse(tablety$liczba_opinii < 50, "mniej 50 opinii",
+                                            ifelse(tablety$liczba_opinii >= 50 & tablety$liczba_opinii <= 100, "50-100 opinii",
+                                            ifelse(tablety$liczba_opinii > 100, "wi�cej 100 opinii", ""))))
+ tablety$status_opinii <- factor(tablety$status_opinii)
+ pie(table(tablety$status_opinii))

#o
> paste(tablety$nazwa, "ma ocen� klient�w", tablety$oceny, "bo ma liczb� opinii", tablety$liczba_opinii)
 [1] "iPad Pro?? ma ocen� klient�w 5 bo ma liczb� opinii 51"               "iPad Air 2 ma ocen� klient�w 4 bo ma liczb� opinii 25"             "iPad Air ma ocen� klient�w 5 bo ma liczb� opinii 265"             
 [4] "4th generation iPad ma ocen� klient�w 5 bo ma liczb� opinii 87"    "3rd generation iPad ma ocen� klient�w 3.5 bo ma liczb� opinii 427" "iPad 2 ma ocen� klient�w 5 bo ma liczb� opinii 34"                
 [7] "iPad mini 4 ma ocen� klient�w 4 bo ma liczb� opinii 98"            "iPad mini 3 ma ocen� klient�w 5 bo ma liczb� opinii 210"           "iPad mini 2 ma ocen� klient�w 3.5 bo ma liczb� opinii 12"         
[10] "iPad mini ma ocen� klient�w 4.5 bo ma liczb� opinii 45"            "iPad Pro 32 ma ocen� klient�w 5 bo ma liczb� opinii 350"           "iPad Pro?? 64 ma ocen� klient�w 4 bo ma liczb� opinii 87"           
[13] "iPad Air 128?? ma ocen� klient�w 4 bo ma liczb� opinii 725"          "iPad Air 2 128?? ma ocen� klient�w 2 bo ma liczb� opinii 660"        "iPad 2 128?? ma ocen� klient�w 5 bo ma liczb� opinii 520"

#p
> write.csv(tablety, file = "Data Frame.csv")
+ dat = read.csv("Data Frame.csv", header = TRUE)